// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDGameMode.generated.h"

/**
 * 
 */
UCLASS()
class TDCPLUSPLUS_API ATDGameMode : public AGameModeBase
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

public:

	FTimerDelegate GracePeriodTimerDel;
	FTimerHandle GracePeriodTimerHan;

	UPROPERTY(EditAnywhere)
		class UTDData* WaveData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int PlayerHealth;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int CurrentWaveNumber;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int EnemiesInWave;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int EnemiesKilled;

	UPROPERTY(EditAnywhere)
		int PlayerMoney;

	UPROPERTY(EditAnywhere)
		TArray<AActor*> Spawners;

	UFUNCTION()
		void SetMoney(int Money);

	UFUNCTION()
		bool CheckMoney(int Price);

	UFUNCTION()
		void SpawnEnemyWaves();

	UFUNCTION()
		void GracePeriodCountdown();

	UFUNCTION()
		void SetHealth();

	UFUNCTION()
		void OnEnemyDeath();

	UFUNCTION()
		void EnemiesToKill();
};
