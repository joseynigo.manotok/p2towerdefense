// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TDData.generated.h"

/**
 *
 */
USTRUCT(BlueprintType)
struct FEnemyClass
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere)
		TSubclassOf<class ATDEnemy> EnemyClass;
};

USTRUCT(BlueprintType)
struct FWave
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere)
		TArray<FEnemyClass> WaveEnemy;
};

UCLASS()
class TDCPLUSPLUS_API UTDData : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere)
		TArray<FWave> Waves;

	UPROPERTY(EditAnywhere)
		float GracePeriod;

	UFUNCTION()
		float GetHealthMod(int WaveNumber);

	UFUNCTION()
		int GetNumberOfEnemiesInWave(int WaveNumber);

	UFUNCTION()
		float GetGracePeriod();

	UFUNCTION()
		int GetMaxWaveNumber();
};
