// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TDTowerAsset.generated.h"

/**
 *
 */
UCLASS()
class TDCPLUSPLUS_API UTDTowerAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<class ATDTowerBase> TowerClasses;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		int TowerPrice;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float TowerRange;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float AttackSpeed;
};
