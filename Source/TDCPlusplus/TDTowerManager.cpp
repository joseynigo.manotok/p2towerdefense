// Fill out your copyright notice in the Description page of Project Settings.


#include "TDTowerManager.h"
#include "TDShopAsset.h"
#include "TDTowerAsset.h"
#include "TDBuildNode.h"
#include "Kismet/GameplayStatics.h"
#include "TDGhostTower.h"
#include "TDGameMode.h"
#include "TDTowerHead.h"


// Sets default values
ATDTowerManager::ATDTowerManager()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

void ATDTowerManager::GetShopPrices(UTexture2D*& TowerIcon, int TargetTowerNumber, int& TargetTowerPrice)
{
	if (TargetTowerNumber < TowerList.Num())
	{
		TowerIcon = TowerList[TargetTowerNumber]->TowerIcon;
		TargetTowerPrice = TowerList[TargetTowerNumber]->TowerData->TowerPrice;
	}
}

void ATDTowerManager::SelectTower(int TowerNumber)
{
	if (TowerNumber >= TowerList.Num())
	{
		return;
	}

	Tower = TowerList[TowerNumber]->TowerData->TowerClasses;

	GhostTower = GetWorld()->SpawnActor<ATDGhostTower>(TowerList[TowerNumber]->GhostTower);

	SelectedTowerPrice = TowerList[TowerNumber]->TowerData->TowerPrice;
}

void ATDTowerManager::BuildTower(ATDBuildNode* BuildNode)
{
	if (Tower != NULL && BuildNode->CheckForTower())
	{
		if (GameMode->CheckMoney(SelectedTowerPrice))
		{
			TowerNode = BuildNode;
			TowerNode->SpawnTower(Tower);
			GameMode->SetMoney(SelectedTowerPrice);
		}
	}
}

// Called when the game starts or when spawned
void ATDTowerManager::BeginPlay()
{
	Super::BeginPlay();
	GameMode = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATDBuildNode::StaticClass(), BuildableNodes);

	for (AActor* Node : BuildableNodes)
	{
		ATDBuildNode* BuildNode = Cast<ATDBuildNode>(Node);
		BuildNode->Click.AddDynamic(this, &ATDTowerManager::BuildTower);
	}
}

// Called every frame
void ATDTowerManager::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

