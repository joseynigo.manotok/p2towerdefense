// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGhostTower.h"
#include "TDBuildNode.h"
#include "TDTowerAsset.h"
#include "Components/SphereComponent.h"

// Sets default values
ATDGhostTower::ATDGhostTower()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root")));

	TowerRange = CreateDefaultSubobject<USphereComponent>("TowerRange");
	TowerRange->SetupAttachment(RootComponent);
}

void ATDGhostTower::AttachToMouse()
{
	FHitResult Hit;

	PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_Visibility, false, Hit);
	SetActorLocation(Hit.Location);

	CheckForNode(Hit);
}

void ATDGhostTower::CheckForNode(FHitResult Hit)
{
	ATDBuildNode* BuildNode = Cast<ATDBuildNode>(Hit.GetActor());

	if (BuildNode)
	{
		UE_LOG(LogTemp, Warning, TEXT("TESTT"));
		if (BuildNode->CheckForTower())
		{
			ChangeColour(Blue);
		}
	}
	else
	{
		ChangeColour(Red);
	}
}

// Called when the game starts or when spawned
void ATDGhostTower::BeginPlay()
{
	Super::BeginPlay();
	PlayerController = GetWorld()->GetFirstPlayerController();

	if (TowerData != nullptr)
	{
		TowerRange->SetSphereRadius(TowerData->TowerRange);
	}
}

// Called every frame
void ATDGhostTower::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	AttachToMouse();
}

