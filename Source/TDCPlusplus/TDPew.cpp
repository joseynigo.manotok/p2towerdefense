// Fill out your copyright notice in the Description page of Project Settings.


#include "TDPew.h"

// Sets default values
ATDPew::ATDPew()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ATDPew::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDPew::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

