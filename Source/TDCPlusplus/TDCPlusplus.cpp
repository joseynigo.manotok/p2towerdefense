// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDCPlusplus.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TDCPlusplus, "TDCPlusplus" );
