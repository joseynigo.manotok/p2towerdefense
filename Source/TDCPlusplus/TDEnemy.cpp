// Fill out your copyright notice in the Description page of Project Settings.


#include "TDEnemy.h"
#include "TDEnemySpawner.h"
#include "TDHealth.h"
#include "TDAIController.h"
#include "TDPlayerCore.h"
#include "TDGameMode.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATDEnemy::ATDEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	EnemyHealth = CreateDefaultSubobject<UTDHealth>("EnemyHealth");
	AddOwnedComponent(EnemyHealth);
}

// Called when the game starts or when spawned
void ATDEnemy::BeginPlay()
{
	Super::BeginPlay();

	ATDGameMode* GameMode = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
	Death.AddDynamic(GameMode, &ATDGameMode::OnEnemyDeath);
}

// Called every frame
void ATDEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void ATDEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ATDEnemy::OnSpawn(float WaveMod)
{
	EnemyHealth->SetHealth(WaveMod);
}

void ATDEnemy::SetSpawner(ATDEnemySpawner* EnemySpawner)
{
	Spawner = EnemySpawner;

	ATDAIController* EnemyController = Cast<ATDAIController>(GetController());
	EnemyController->SetSpawner(Spawner);
}

void ATDEnemy::Kill()
{
	Death.Broadcast();
	Destroy();
}

void ATDEnemy::TakeTowerDamage(int TowerDamage)
{
	UE_LOG(LogTemp, Warning, TEXT("EnemyDamaged"));
	EnemyHealth->Damage(TowerDamage);

	if (EnemyHealth->Health < 0)
	{
		Kill();
	}
}
