// Fill out your copyright notice in the Description page of Project Settings.


#include "TDBuildNode.h"
#include "TDTowerHead.h"
#include "Components/ArrowComponent.h"

// Sets default values
ATDBuildNode::ATDBuildNode()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root")));

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	StaticMesh->SetupAttachment(RootComponent);

	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Spawn"));
	SpawnPoint->SetupAttachment(RootComponent);
}

bool ATDBuildNode::CheckForTower()
{
	if (Tower == nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("NoTower"));
		return true;
	}

	return false;
}

void ATDBuildNode::SpawnTower(TSubclassOf<class ATDTowerHead> BuildTower)
{
	TowerClass = BuildTower;
	if (TowerClass)
	{
		Tower = GetWorld()->SpawnActor<ATDTowerHead>(TowerClass, SpawnPoint->GetComponentTransform());
	}
}

void ATDBuildNode::NodeClick(UPrimitiveComponent* BuildNode, FKey ButtonPressed)
{
	UE_LOG(LogTemp, Warning, TEXT("Clicky"));
	Click.Broadcast(this);
}

// Called when the game starts or when spawned
void ATDBuildNode::BeginPlay()
{
	Super::BeginPlay();
	StaticMesh->OnClicked.AddDynamic(this, &ATDBuildNode::NodeClick);
}

// Called every frame
void ATDBuildNode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

