// Fill out your copyright notice in the Description page of Project Settings.


#include "TDTowerHead.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Components/ArrowComponent.h"
#include "TDEnemy.h"
#include "TDTowerProjectile.h"
#include <Runtime/Engine/Classes/Kismet/KismetMathLibrary.h>


ATDTowerHead::ATDTowerHead()
{
	Gun = CreateDefaultSubobject<UStaticMeshComponent>("Gun");
	Gun->SetupAttachment(TurretBase);

	ProjectileSpawn = CreateDefaultSubobject<UArrowComponent>("ProjectileSpawn");
	ProjectileSpawn->SetupAttachment(Gun);
}

void ATDTowerHead::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	SetTarget();
}

void ATDTowerHead::LookAtTarget()
{
	if (EnemyTarget)
	{
		FRotator Rot = UKismetMathLibrary::FindLookAtRotation(this->GetActorLocation(), EnemyTarget->GetActorLocation());
		Gun->SetWorldRotation(Rot);
	}
}

void ATDTowerHead::Fire()
{
	FireDelayDel.BindUFunction(this, FName("FireProjectile"));
	GetWorld()->GetTimerManager().SetTimer(FireDelayHan, FireDelayDel, AttackSpeed, false);
}

void ATDTowerHead::FireProjectile()
{
	if (EnemyTarget)
	{
		UE_LOG(LogTemp, Warning, TEXT("ProjectileSpawned"));
		ATDTowerProjectile* Projectile = GetWorld()->SpawnActor<ATDTowerProjectile>(TowerProjectile, ProjectileSpawn->GetComponentTransform());
		Projectile->SetDamage(TowerDamage);

		FRotator Rot = UKismetMathLibrary::FindLookAtRotation(Projectile->GetActorLocation(), EnemyTarget->GetActorLocation());
		Projectile->SetActorRotation(Rot);
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Test"));
	}
}

void ATDTowerHead::SetTarget()
{
	if (EnemyList.Num() > 0)
	{
		EnemyTarget = Cast<ATDEnemy>(EnemyList[0]);
		LookAtTarget();
		Fire();
	}
}

void ATDTowerHead::TowerRadiusComponentOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UE_LOG(LogTemp, Warning, TEXT("TargetAquired"));
	if (ATDEnemy* Target = Cast<ATDEnemy>(OtherActor))
	{
		EnemyList.Add(Target);

		//test
		FireProjectile();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Test"));
	}
}

void ATDTowerHead::TowerRadiusComponentOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
	if (ATDEnemy* Target = Cast<ATDEnemy>(OtherActor))
	{
		EnemyTarget = nullptr;
		EnemyList.Remove(Target);
		SetTarget();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Test"));
	}
}
