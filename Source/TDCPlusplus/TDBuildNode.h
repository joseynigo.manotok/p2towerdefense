// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDBuildNode.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnClick, ATDBuildNode*, Node);

UCLASS()
class TDCPLUSPLUS_API ATDBuildNode : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDBuildNode();

	UFUNCTION()
		void NodeClick(UPrimitiveComponent* BuildNode, FKey ButtonPressed);

	UFUNCTION()
		bool CheckForTower();
	
	UFUNCTION()
		void SpawnTower(TSubclassOf<class ATDTowerHead> BuildTower);

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere)
		class UArrowComponent* SpawnPoint;

	UPROPERTY(BlueprintAssignable, BlueprintReadWrite)
		FOnClick Click;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class ATDTowerHead> TowerClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class ATDTowerBase* Tower;

private:


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
