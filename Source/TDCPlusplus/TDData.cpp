// Fill out your copyright notice in the Description page of Project Settings.


#include "TDData.h"

float UTDData::GetHealthMod(int WaveNumber)
{
	return WaveNumber / 10;
}

int UTDData::GetNumberOfEnemiesInWave(int WaveNumber)
{
	return Waves[WaveNumber].WaveEnemy.Num();
}

float UTDData::GetGracePeriod()
{
	return GracePeriod;
}

int UTDData::GetMaxWaveNumber()
{
	return Waves.Num();
}
