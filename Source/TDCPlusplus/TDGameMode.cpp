// Fill out your copyright notice in the Description page of Project Settings.


#include "TDGameMode.h"
#include "TDEnemySpawner.h"
#include "Kismet/GameplayStatics.h"
#include "TDData.h"

void ATDGameMode::BeginPlay()
{
	Super::BeginPlay();

	PlayerMoney = 500;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ATDEnemySpawner::StaticClass(), Spawners);
	GracePeriodCountdown();
}

void ATDGameMode::SetMoney(int Money)
{
	PlayerMoney += Money;
}

bool ATDGameMode::CheckMoney(int Price)
{
	if (PlayerMoney >= Price)
	{
		return true;
	}
	return false;
}

void ATDGameMode::SpawnEnemyWaves()
{
	UE_LOG(LogTemp, Warning, TEXT("SpawnWaves"));
	EnemiesToKill();
	for (AActor* SpawnerActor : Spawners)
	{
		ATDEnemySpawner* EnemySpawner = Cast<ATDEnemySpawner>(SpawnerActor);
		EnemySpawner->SpawnWave(CurrentWaveNumber);
	}

}

void ATDGameMode::GracePeriodCountdown()
{
	GracePeriodTimerDel.BindUFunction(this, FName("SpawnEnemyWaves"));
	GetWorld()->GetTimerManager().SetTimer(GracePeriodTimerHan, GracePeriodTimerDel, WaveData->GetGracePeriod(), false);
}

void ATDGameMode::SetHealth()
{
	PlayerHealth -= 1;
}

void ATDGameMode::OnEnemyDeath()
{
	EnemiesKilled++;
	SetMoney(CurrentWaveNumber + 1);
	if (EnemiesKilled >= EnemiesInWave)
	{
		CurrentWaveNumber++;
		GracePeriodCountdown();
	}
}

void ATDGameMode::EnemiesToKill()
{
	EnemiesInWave = 0;
	EnemiesKilled = 0;

	for (AActor* SpawnerActor : Spawners)
	{
		EnemiesInWave += WaveData->GetNumberOfEnemiesInWave(CurrentWaveNumber);
	}
}
