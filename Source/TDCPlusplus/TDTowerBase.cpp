// Fill out your copyright notice in the Description page of Project Settings.


#include "TDTowerBase.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ATDTowerBase::ATDTowerBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root")));

	TurretBase = CreateDefaultSubobject<UStaticMeshComponent>("TurretBase");
	TurretBase->SetupAttachment(RootComponent);

	TowerRadius = CreateDefaultSubobject<USphereComponent>("TowerRadius");
	TowerRadius->SetupAttachment(RootComponent);

	BuildRadius = CreateDefaultSubobject<USphereComponent>("BuildRadius");
	BuildRadius->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATDTowerBase::BeginPlay()
{
	Super::BeginPlay();
	
	TowerRadius->OnComponentBeginOverlap.AddDynamic(this, &ATDTowerBase::TowerRadiusComponentOverlap);
	TowerRadius->OnComponentEndOverlap.AddDynamic(this, &ATDTowerBase::TowerRadiusComponentOverlapEnd);
}

// Called every frame
void ATDTowerBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDTowerBase::TowerRadiusComponentOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ATDTowerBase::TowerRadiusComponentOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody)
{
}

