// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDGhostTower.generated.h"

UCLASS()
class TDCPLUSPLUS_API ATDGhostTower : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDGhostTower();

	UFUNCTION()
		void AttachToMouse();

	UFUNCTION()
		void CheckForNode(FHitResult Hit);

	UFUNCTION(BlueprintImplementableEvent)
		void ChangeColour(UMaterial* Mat);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UTDTowerAsset* TowerData;

	UPROPERTY(EditAnywhere)
		class USphereComponent* TowerRange;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		APlayerController* PlayerController;

	UPROPERTY(EditAnywhere)
		UMaterial* Blue;

	UPROPERTY(EditAnywhere)
		UMaterial* Red;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
