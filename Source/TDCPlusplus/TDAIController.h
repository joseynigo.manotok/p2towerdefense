// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TDAIController.generated.h"

/**
 * 
 */
UCLASS()
class TDCPLUSPLUS_API ATDAIController : public AAIController
{
	GENERATED_BODY()

		void OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result) override;

public:

	UFUNCTION()
		void MoveToWaypoint();

	UFUNCTION()
		void SetSpawner(class ATDEnemySpawner* Spawner);

private:

	UPROPERTY()
		class ATDEnemySpawner* EnemySpawner;

	UPROPERTY()
		int PathNumber;

	UPROPERTY()
		int WaypointNumber;
};
