// Fill out your copyright notice in the Description page of Project Settings.


#include "TDTowerProjectile.h"
#include "TDEnemy.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"

// Sets default values
ATDTowerProjectile::ATDTowerProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>("Root"));

	Projectile = CreateDefaultSubobject<UStaticMeshComponent>("Bullet");
	Projectile->SetupAttachment(RootComponent);

	Projectile->OnComponentBeginOverlap.AddDynamic(this, &ATDTowerProjectile::HitEnemy);

	PewPew = CreateDefaultSubobject<UProjectileMovementComponent>("Movement");
	AddOwnedComponent(PewPew);

	TimeOfDeath = 10.0f;
}

void ATDTowerProjectile::HitEnemy(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ATDEnemy* Enemy = Cast<ATDEnemy>(OtherActor))
	{
		Enemy->TakeTowerDamage(Damage);
		KillBullet();
	}
}

void ATDTowerProjectile::KillBullet()
{
	Destroy();
}

void ATDTowerProjectile::SetDamage(int TowerDamage)
{
	Damage = TowerDamage;
}

void ATDTowerProjectile::TakeAwayLifeSupport()
{
	ExpirationDel.BindUFunction(this, FName("KillBullet"));
	GetWorld()->GetTimerManager().SetTimer(ExpirationHan, ExpirationDel, TimeOfDeath, false);
}

// Called when the game starts or when spawned
void ATDTowerProjectile::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATDTowerProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

