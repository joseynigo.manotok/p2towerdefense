// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "TDShopAsset.generated.h"

/**
 *
 */
UCLASS()
class TDCPLUSPLUS_API UTDShopAsset : public UDataAsset
{
	GENERATED_BODY()

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UTDTowerAsset* TowerData;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UTexture2D* TowerIcon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATDTowerBase> Tower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TSubclassOf<class ATDGhostTower> GhostTower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int Price;
};
