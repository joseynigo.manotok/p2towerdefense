// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "TDCPlusplusGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class TDCPLUSPLUS_API ATDCPlusplusGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
