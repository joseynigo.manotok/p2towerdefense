// Fill out your copyright notice in the Description page of Project Settings.


#include "TDEnemySpawner.h"
#include "Kismet/GameplayStatics.h"
#include "TDWaypoint.h"
#include "TDEnemy.h"
#include "Components/ArrowComponent.h"
#include "TDData.h"

// Sets default values
ATDEnemySpawner::ATDEnemySpawner()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root")));

	SpawnPoint = CreateDefaultSubobject<UArrowComponent>(TEXT("Spawn"));
	SpawnPoint->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATDEnemySpawner::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ATDEnemySpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDEnemySpawner::SpawnEnemy(int WaveNumber)
{
	UE_LOG(LogTemp, Warning, TEXT("EnemySpawned"));
	ATDEnemy* Enemy = GetWorld()->SpawnActor<ATDEnemy>(WaveData->Waves[WaveNumber].WaveEnemy[EnemyNum].EnemyClass, SpawnPoint->GetComponentTransform());
	Enemy->SetSpawner(this);
	Enemy->OnSpawn(WaveData->GetHealthMod(WaveNumber));
	EnemyNum++;
}

void ATDEnemySpawner::SpawnWave(int WaveNumber)
{
	if (EnemyNum < WaveData->GetNumberOfEnemiesInWave(WaveNumber))
	{
		SpawnEnemy(WaveNumber);
		SpawnDelayDel.BindUFunction(this, FName("SpawnWave"), WaveNumber);
		GetWorld()->GetTimerManager().SetTimer(SpawnDelayHan, SpawnDelayDel, 1.0f, false);
	}
	else
	{
		EnemyNum = 0;
	}
}

AActor* ATDEnemySpawner::GetWaypoint(int PathNumber, int WaypointNumber)
{
	if (WaypointNumber <= Paths[PathNumber].Waypoints.Num() - 1)
	{
		return Paths[PathNumber].Waypoints[WaypointNumber];
	}
	else
	{
		return Paths[PathNumber].Waypoints[GetMaxWaypointNumber(PathNumber)];
	}
}

int ATDEnemySpawner::GetMaxWaypointNumber(int PathNumber)
{
	return Paths[PathNumber].Waypoints.Num() - 1;
}

int ATDEnemySpawner::GetRandomPathNumber()
{
	return FMath::RandRange(0, Paths.Num() - 1);
}