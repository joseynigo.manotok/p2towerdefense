// Fill out your copyright notice in the Description page of Project Settings.


#include "TDHealth.h"

// Sets default values for this component's properties
UTDHealth::UTDHealth()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UTDHealth::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UTDHealth::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTDHealth::SetHealth(float WaveMod)
{
	Health = BaseHealth + WaveMod;
}

void UTDHealth::Damage(int TowerDamage)
{
	Health -= TowerDamage;
}

