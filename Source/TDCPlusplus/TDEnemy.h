// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDEnemy.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeath);

UCLASS()
class TDCPLUSPLUS_API ATDEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ATDEnemy();

	UPROPERTY()
		FOnDeath Death;

	UPROPERTY(EditAnywhere)
		class UTDHealth* EnemyHealth;

	UPROPERTY()
		class ATDEnemySpawner* Spawner;

	UPROPERTY()
		AActor* Waypoint;

	UPROPERTY()
		int WaypointNumber;

	UPROPERTY()
		int PathNumber;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION()
		void OnSpawn(float WaveMod);

	UFUNCTION()
		void SetSpawner(class ATDEnemySpawner* EnemySpawner);

	UFUNCTION()
		void Kill();

	UFUNCTION()
		void TakeTowerDamage(int TowerDamage);
};
