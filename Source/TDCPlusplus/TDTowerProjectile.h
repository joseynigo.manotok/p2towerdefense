// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDTowerProjectile.generated.h"

UCLASS()
class TDCPLUSPLUS_API ATDTowerProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATDTowerProjectile();

	UFUNCTION()
		void HitEnemy(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void KillBullet();

	UFUNCTION()
		void SetDamage(int TowerDamage);

	UFUNCTION()
		void TakeAwayLifeSupport();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	FTimerDelegate ExpirationDel;
	FTimerHandle ExpirationHan;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* Projectile;

	UPROPERTY(EditAnywhere)
		class UProjectileMovementComponent* PewPew;
	
	UPROPERTY(EditAnywhere)
		float BulletTime;

	UPROPERTY(EditAnywhere)
		int Damage;

	UPROPERTY(EditAnywhere)
		float TimeOfDeath;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
