// Fill out your copyright notice in the Description page of Project Settings.


#include "TDPlayerCore.h"
#include "TDEnemy.h"
#include "Kismet/GameplayStatics.h"
#include "Components/BoxComponent.h"
#include "TDGameMode.h"

// Sets default values
ATDPlayerCore::ATDPlayerCore()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SetRootComponent(CreateDefaultSubobject<USceneComponent>(TEXT("Root")));

	HitBox = CreateDefaultSubobject<UBoxComponent>(TEXT("HitBox"));
	HitBox->SetupAttachment(RootComponent);
}

// Called when the game starts or when spawned
void ATDPlayerCore::BeginPlay()
{
	Super::BeginPlay();
	
	HitBox->OnComponentBeginOverlap.AddDynamic(this, &ATDPlayerCore::EnemyHit);
}

// Called every frame
void ATDPlayerCore::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATDPlayerCore::EnemyHit(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (ATDEnemy* Enemy = Cast<ATDEnemy>(OtherActor))
	{
		ATDGameMode* GameMode = Cast<ATDGameMode>(UGameplayStatics::GetGameMode(GetWorld()));
		GameMode->SetHealth();
		Damage.Broadcast();
		Enemy->Kill();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("SpawnWaves"));
	}
}

