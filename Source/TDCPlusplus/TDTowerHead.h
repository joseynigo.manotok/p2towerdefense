// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TDTowerBase.h"
#include "TDTowerHead.generated.h"

/**
 *
 */
UCLASS()
class TDCPLUSPLUS_API ATDTowerHead : public ATDTowerBase
{
	GENERATED_BODY()

public:

	ATDTowerHead();

	virtual void Tick(float DeltaTime) override;

protected:

	FTimerDelegate FireDelayDel;
	FTimerHandle FireDelayHan;

	UPROPERTY(EditAnywhere)
		class UStaticMeshComponent* Gun;

	UPROPERTY(EditAnywhere)
		AActor* EnemyTarget;

	UPROPERTY(EditAnywhere)
		class UArrowComponent* ProjectileSpawn;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class ATDTowerProjectile> TowerProjectile;

	UPROPERTY(EditAnywhere)
		float AttackSpeed;

	UPROPERTY(EditAnywhere)
		int TowerDamage;

	UFUNCTION()
		void LookAtTarget();

	UFUNCTION()
		void FireProjectile();

	UFUNCTION()
		void Fire();

	UFUNCTION()
		void SetTarget();

	virtual void TowerRadiusComponentOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;

	virtual void TowerRadiusComponentOverlapEnd(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBody) override;
};
