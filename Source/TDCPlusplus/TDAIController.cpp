// Fill out your copyright notice in the Description page of Project Settings.


#include "TDAIController.h"
#include "TDEnemySpawner.h"

void ATDAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	WaypointNumber++;
	MoveToWaypoint();
}

void ATDAIController::MoveToWaypoint()
{
	if (WaypointNumber > EnemySpawner->GetMaxWaypointNumber(PathNumber))
	{
		return;
	}

	MoveToActor(EnemySpawner->GetWaypoint(PathNumber, WaypointNumber));
}

void ATDAIController::SetSpawner(ATDEnemySpawner* Spawner)
{
	EnemySpawner = Spawner;
	PathNumber = Spawner->GetRandomPathNumber();
	MoveToWaypoint();
}
