// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDTowerManager.generated.h"

UCLASS()
class TDCPLUSPLUS_API ATDTowerManager : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDTowerManager();

	UFUNCTION(BlueprintCallable)
		void GetShopPrices(UTexture2D*& TowerIcon, int TowerNumber, int& TowerPrice);

	UFUNCTION(BlueprintCallable)
		void SelectTower(int TowerNumber);

	UFUNCTION()
		void BuildTower(ATDBuildNode* BuildNode);

	UPROPERTY(EditAnywhere)
		TArray<class UTDShopAsset*> TowerList;

	UPROPERTY(EditAnywhere)
		TArray<AActor*> BuildableNodes;

	UPROPERTY(EditAnywhere)
		class ATDBuildNode* TowerNode;

	UPROPERTY(EditAnywhere)
		class ATDGameMode* GameMode;

	UPROPERTY(EditAnywhere)
		class ATDGhostTower* GhostTower;

	UPROPERTY(EditAnywhere)
		TSubclassOf<class ATDTowerHead> Tower;

	UPROPERTY(EditAnywhere)
		int SelectedTowerPrice;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
