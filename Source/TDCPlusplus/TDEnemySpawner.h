// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TDEnemySpawner.generated.h"

USTRUCT(BlueprintType)
struct FPath
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere)
		TArray<class ATDWaypoint*> Waypoints;
};

UCLASS()
class TDCPLUSPLUS_API ATDEnemySpawner : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ATDEnemySpawner();


private:

	FTimerDelegate SpawnDelayDel;
	FTimerHandle SpawnDelayHan;

	UPROPERTY(EditAnywhere)
		class UArrowComponent* SpawnPoint;

	UPROPERTY(EditAnywhere)
		class UTDData* WaveData;

	UPROPERTY(EditAnywhere)
		int EnemyNum;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere)
		TArray<FPath> Paths;

	UFUNCTION()
		void SpawnEnemy(int WaveNumber);

	UFUNCTION()
		void SpawnWave(int WaveNumber);

	UFUNCTION()
		AActor* GetWaypoint(int PathNumber, int WaypointNumber);

	UFUNCTION()
		int GetMaxWaypointNumber(int PathNumber);

	UFUNCTION()
		int GetRandomPathNumber();
};
